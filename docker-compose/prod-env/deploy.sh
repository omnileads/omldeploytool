                                                                                                                                                                                                                                                                                                                                                                                                    #!/bin/bash

set -e

######################################################
####################  NETWORKING #####################
######################################################

oml_nic=${NIC}
lan_addr=${PRIVATE_IPV4}
wan_addr=${PUBLIC_IPV4}

######################################################
###################### STAGE #########################
######################################################

env=docker

# --- branch is about specific omnileads release
branch=${BRANCH:-main}  # Default to "main" if not specified

######################################################
##### External Object Storage Bucket integration #####
######################################################
bucket_url=${BUCKET_URL}
bucket_access_key=${BUCKET_ACCESS_KEY}
bucket_secret_key=${BUCKET_SECRET_KEY}
bucket_region=${BUCKET_REGION}
bucket_name=${BUCKET_NAME}

# External PostgreSQL engine integration
postgres_host=${PGSQL_HOST}
postgres_port=${PGSQL_PORT}
postgres_user=${PGSQL_USER}
postgres_password=${PGSQL_PASSWORD}
postgres_db=${PGDATABASE}

######################################################
###################### FUNC ##########################
######################################################

log_info() {
    echo -e "\033[0;32m$1\033[0m"  # Mensaje en verde
}

log_error() {
    echo -e "\033[0;31m$1\033[0m" >&2  # Mensaje en rojo
    exit 1
}

setup_networking() {
    log_info "*** Network settings ***"
    if [[ -z "$lan_addr" ]]; then
        lan_addr=$(ip addr show "$oml_nic" | grep "inet\b" | awk '{print $2}' | cut -d/ -f1) || log_error "No se pudo obtener la dirección privada."
    else
        lan_addr="$lan_addr"
    fi

    if [[ -z "$wan_addr" ]]; then
        wan_addr=$(curl -s http://ipinfo.io/ip) || log_error "No se pudo obtener la dirección pública."
    fi
}

setup_os_dependencies() {
    log_info "*** Install docker and others dependencies ***"
    if [ -f /etc/os-release ]; then
        . /etc/os-release
        OS_ID=$ID
    else
        log_error "No se puede determinar el sistema operativo."
    fi

    if [[ "$OS_ID" =~ (debian|ubuntu) ]]; then
        apt update && apt install -y git curl
        curl -fsSL https://get.docker.com -o ~/get-docker.sh
        bash ~/get-docker.sh
    elif [[ "$OS_ID" =~ (rhel|almalinux|rocky|centos) ]]; then
        dnf check-update
        dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
        dnf install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin git
        systemctl start docker
        systemctl enable docker
    else
        log_error "Linux distro not found. Please install Docker manually."
    fi

    ln -sf /usr/libexec/docker/cli-plugins/docker-compose /usr/bin/docker-compose
}

deploy_omnileads() {
    log_info "Cloning the OML deploy tool repository"
    git clone https://gitlab.com/omnileads/omldeploytool.git || log_error "Error al clonar el repositorio."

    cd omldeploytool || log_error "Canot access the 'omldeploytool' directory."
    
    if [[ "$branch" != "main" ]]; then
        git checkout "$branch" || log_error "Error al cambiar a la rama '$branch'."
    fi
    
    cp docker-compose/oml_manage /usr/local/bin/oml_manage
    cd docker-compose/prod-env || log_error "Canot access the 'prod-env' directory."

    cp ../env ./.env
    sed -i "s/ENV=devenv/ENV=${env}/g" .env
    sed -i "s/PRIVATE_IP=/PRIVATE_IP=${lan_addr}/g" .env
    sed -i "s/PUBLIC_IP=/PUBLIC_IP=${wan_addr}/g" .env

    docker-compose up -d || log_error "Error while executing docker-compose up -d."
}

setup_iptables() {
    log_info "Iptables RTP rules setup"

    # Crear reglas de iptables
    iptables -t nat -A PREROUTING -p udp --dport 5060 -j DNAT --to-destination 10.22.22.99
    iptables -A FORWARD -p udp -d 10.22.22.99 --dport 5060 -j ACCEPT
    iptables -t nat -A PREROUTING -p udp --dport 40000:50000 -j DNAT --to-destination 10.22.22.99
    iptables -A FORWARD -p udp -d 10.22.22.99 --dport 40000:50000 -j ACCEPT
    iptables -t nat -A PREROUTING -p udp --dport 20000:30000 -j DNAT --to-destination 10.22.22.98
    iptables -A FORWARD -p udp -d 10.22.22.98 --dport 20000:30000 -j ACCEPT

    # Crear o modificar rc.local
    if [[ ! -f /etc/rc.local ]]; then
        echo -e "#!/bin/bash\nexit 0" > /etc/rc.local
        chmod +x /etc/rc.local
    fi

    # Añadir reglas a rc.local si no existen
    if ! grep -q "iptables -t nat -A PREROUTING -p udp --dport 5060" /etc/rc.local; then
        sed -i '/^exit 0$/i \
iptables -t nat -A PREROUTING -p udp --dport 5060 -j DNAT --to-destination 10.22.22.99\n\
iptables -A FORWARD -p udp -d 10.22.22.99 --dport 5060 -j ACCEPT\n\
iptables -t nat -A PREROUTING -p udp --dport 40000:50000 -j DNAT --to-destination 10.22.22.99\n\
iptables -A FORWARD -p udp -d 10.22.22.99 --dport 40000:50000 -j ACCEPT\n\
iptables -t nat -A PREROUTING -p udp --dport 20000:30000 -j DNAT --to-destination 10.22.22.98\n\
iptables -A FORWARD -p udp -d 10.22.22.98 --dport 20000:30000 -j ACCEPT' /etc/rc.local
        log_info "Reglas de iptables añadidas a rc.local."
    else
        log_info "The iptables rules already exist in rc.local."
    fi
}

wait_for_env() {
    log_info "*** The environment is currently starting up. Please wait. ***"
    until curl -sk --head --request GET "https://${lan_ipv4}" | grep "302" > /dev/null; do
        log_info "The system is in the process of starting up. Please wait ..."
        sleep 10
    done
    log_info "¡Deploy ready!"
}

reset_admin_password() {
    log_info "*** Password reset ***"
    /usr/local/bin/oml_manage --reset_pass || log_error "Error al resetear la contraseña de administrador."
}

######################################################
####################### EXEC #########################
######################################################

setup_networking
setup_os_dependencies
deploy_omnileads
setup_iptables
wait_for_env
reset_admin_password
