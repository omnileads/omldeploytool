# Release Notes - OMniLeads 2.3.1
[2025-02-06]

## Added

## Improvements

## Fixes

* oml-712 Fix on Kamailio environments variables.

## Component changes
